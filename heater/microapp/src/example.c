#include <Arduino.h>
#include <Presence.h>

Presence presence;

uint8_t profileId = 0;
uint8_t roomId = 0;

// For some reason a delay in Bluenet seems to be 3 times as high as it should be
uint8_t delay_correction = 0.3;
uint8_t second = 1000 * delay_correction;

// 1800 seconds, half an hour
//uint8_t delay_time = 1800;
// 10 seconds to test app
uint8_t delay_time = 20;

#define HEADER_SWITCH CS_MICROAPP_COMMAND_PIN_SWITCH

uint8_t serviceDataBuf[12] = {0};
uint8_t isPresent = 0;
uint8_t timer = 0;
uint8_t heater_state = 0;

void setup() {
	Serial.begin();

	// Set the UUID of this microapp.
	serviceDataBuf[0] = 24;
	serviceDataBuf[1] = 49;
}


void loop() {

	isPresent = presence.isPresent(profileId, roomId);
	if(isPresent){
		if(timer < delay_time){
			heater_state = 1;
			Serial.print("Seconds heated");
			Serial.println(timer);
		}
		else{
			Serial.println("Done heating");
			heater_state = 2;
		}

		timer++;
	}
	else{
		Serial.println("User left sphere");
		heater_state = 0;
		timer = 0;
	}
	Serial.print("Heater state: ");
	Serial.println(heater_state);

	// Note: We automaticly have a 1 second delay when the loop is called

	if(heater_state == 1)
		digitalWrite(HEADER_SWITCH, HIGH);
	else
		digitalWrite(HEADER_SWITCH, LOW);

	serviceDataBuf[2] = isPresent;
	serviceDataBuf[3] = heater_state;
	serviceDataBuf[4] = timer;
	SerialServiceData.write(serviceDataBuf, sizeof(serviceDataBuf));

	return;
}
