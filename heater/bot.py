#!/usr/bin/env python3

import json
import asyncio
import requests
from crownstone_ble import CrownstoneBle

from crownstone_core.packets.serviceDataParsers.containers.elements.AdvTypes import AdvType
from crownstone_ble.core.container.ScanData import ScanData
from crownstone_ble import CrownstoneBle, BleEventBus, BleTopics

webhooks = "https://discord.com/api/webhooks/928670233487630416/thAhsBIievHiVhAtTu8CLOIxXky_EDYVxPEpxZaEy1-dEZWEcXQlTJRgyCEsiUwWLnvh"

core = CrownstoneBle(bleAdapterAddress="00:1A:7D:DA:71:13")
sphere = open("sphere.json")
core.loadSettingsFromDictionary(json.load(sphere))

previeus_heater_state = 0;
previeus_presence = 0;

def printAdvertisements(data: ScanData):

    global previeus_heater_state
    global previeus_presence
    
    if data.payload.type == AdvType.MICROAPP_DATA:

        presence = data.payload.microappData[0]
        heater_state = data.payload.microappData[1]
        timer = data.payload.microappData[2]

        print(f"previeus_presence: {previeus_presence} presence: {presence}, ", end ="")
        print(f"heater_state: {heater_state}, timer: {timer}")

        if previeus_presence == 0 and presence == 1:
            print("User entered room, heating up room")
            previeus_presence = 1
        elif previeus_presence == 1 and presence == 0:
            print("User left room, stop heating")
            previeus_presence = 0

        if previeus_heater_state != 2 and heater_state == 2:
            print("Room is heated, turn heater off")
            previeus_heater_state = 2
        elif previeus_heater_state == 2 and heater_state != 2:
            print("Reset")
            previeus_heater_state = 0

BleEventBus.subscribe(BleTopics.advertisement, printAdvertisements)

async def main():

    await core.ble.scan(duration=600)
    await core.disconnect()
    await core.shutDown()

try:
    asyncio.run(main())
except KeyboardInterrupt:
    print("Stopping.")
