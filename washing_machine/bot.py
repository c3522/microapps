#!/usr/bin/env python3

import json
import asyncio
import requests
from crownstone_ble import CrownstoneBle

from crownstone_core.packets.serviceDataParsers.containers.elements.AdvTypes import AdvType
from crownstone_ble.core.container.ScanData import ScanData
from crownstone_ble import CrownstoneBle, BleEventBus, BleTopics

webhook = "https://discord.com/api/webhooks/928590595977527306/QxaRVcOghCHAopX23M9MSoVKq08WNlbsxAXp8ANMyoa_oosij52MwFemWEQvrfaX6sCp"

core = CrownstoneBle(bleAdapterAddress="00:1A:7D:DA:71:13")
sphere = open("sphere.json")
core.loadSettingsFromDictionary(json.load(sphere))

previeus_washing_machine_state = None;

def printAdvertisements(data: ScanData):
    
    global previeus_washing_machine_state

    if data.payload.type == AdvType.MICROAPP_DATA:

        washing_machine_state = data.payload.microappData[0]
        power_usage_value = data.payload.microappData[1]

        if washing_machine_state == 1 and previeus_washing_machine_state != 1:
            print(f"Machine is running")
            reply = requests.post(webhook , data={'content': "De thee word gemaakt"})
            previeus_washing_machine_state = 1
        elif washing_machine_state == 0 and previeus_washing_machine_state != 0:
            print(f"Machine not running")
            reply = requests.post(webhook , data={'content': "De thee is klaar"})
            previeus_washing_machine_state = 0

BleEventBus.subscribe(BleTopics.advertisement, printAdvertisements)

async def main():

    await core.ble.scan(duration=600)
    await core.disconnect()
    await core.shutDown()

try:
    asyncio.run(main())
except KeyboardInterrupt:
    print("Stopping.")
