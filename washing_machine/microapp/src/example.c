#include <Arduino.h>
#include <PowerUsage.h>

PowerUsage powerUsage;
static int washing_machine_state = 0;

uint8_t serviceDataBuf[12] = {0};

// For some reason a delay in Bluenet seems to be 3 times as high as it should be
float delay_correction = 0.3;
uint8_t second = 1000 * delay_correction;

uint8_t seconds_no_power = 0;

void setup() {
	Serial.begin();

	// Set the UUID of this microapp.
	serviceDataBuf[0] = 24;
	serviceDataBuf[1] = 49;
}

void loop() {

	// Dummy function to switch states used for testing
	// if(washing_machine_state==1) washing_machine_state=0;
	// else washing_machine_state=1;

	int32_t milli_watts = powerUsage.getPowerUsageInMilliWatts();
	Serial.print("Power usage microapp: ");
	Serial.println(milli_watts);

	if(milli_watts < 2 * 1000){

		if(seconds_no_power > 60 && washing_machine_state == 1){
			washing_machine_state = 0;
			seconds_no_power = 0;
			Serial.println("Washing machine is done");
		}
		seconds_no_power++;

	}
	else {
		washing_machine_state=1;
	}

	Serial.print("Washing machine state: ");
	Serial.println(washing_machine_state);

	delay(second);

	serviceDataBuf[2] = washing_machine_state;
	serviceDataBuf[3] = milli_watts;
	SerialServiceData.write(serviceDataBuf, sizeof(serviceDataBuf));

	return;
}
