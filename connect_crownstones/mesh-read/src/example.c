#include <Arduino.h>
#include <Presence.h>
#include <Mesh.h>
#include <PowerUsage.h>

//PowerUsage powerUsage;
//Presence presence;
Mesh mesh;

void setup() {
}

void loop() {

	// Read Mesh
	if(mesh.available()){
		Serial.println("Got msg");
		uint8_t * msg_ptr = nullptr;
		uint8_t stone_id = 0;

		//uint8_t dummy_arr[MICROAPP_MAX_MESH_MESSAGE_SIZE] = {5,5,5,5,5,5,5};

		uint8_t size = mesh.readMeshMsg(&msg_ptr, &stone_id); 

		Serial.print("Stone Id: ");
		Serial.print((short)stone_id);
		Serial.print("Receive msg: ");
		while(size-- != 0){
			Serial.print((short)*msg_ptr++);
			//Serial.print((short)dummy_arr[size]);
			Serial.print(" : ");
		}
		Serial.println("end");
	}
	//uint8_t msg[7] = {8,8,8,8,8,8,8};
	//uint8_t stoneId = 0;
	//mesh.sendMeshMsg(msg, sizeof(msg), stoneId);
}
