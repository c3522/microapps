# Microapps

This reposetory contains a Microapps written for the Crownstone Plugs.
The following apps can be found in the reposetory:


## Washing Machine / Detect when device is done

This Microapp work together with a Python script to log the results of the app.
The Microapp measures the wattage of the device its connected to. When the threashold is lower than a specific value for a specific amount of time the app will send a Bluetooth advertisement (service data) that it is in a "done" state.
Before going into this state the device first has to measure that electricity is being used.

The application is mend to recognise a washing machine, altho it can also recognise other devices like a water heater, dish washer etc.

## Automatic heater

This Microapp solves the usecase of a specific problem I'm having.
When I enter my room I want the heater to go on, and after half an hour I want it to go off again, since I don't want to sleep in a room that is too warm.

This app measures the user presence to determine if the heater has to go on.
The app also uses delays to wait for half an hour (or any other specified time), altho there are some complications with the expected time with the delays. Because of this problem we can't define a strict half hour in the Microapp.

## Connect Crownstones

There were some problems with the API mesh feature. The app folder contains code which can test the functionality of meshing itself, showing that the wrong data is received. But this code does not connect Crownstones.
